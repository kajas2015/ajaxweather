
//TO DO https://openweathermap.org/current#data predelat data na aktualni
let header = document.querySelector('h2');
let text = document.querySelectorAll('p');
let img = document.querySelector('img');
let input = document.querySelector('input');
let welcomeMsg = document.querySelector('#welcome');


input.addEventListener("keydown", function (e) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        // Trigger the button element with a click
        loadLocation(input.value);

    } else {
    }
});

function loadLocation(input) {
    console.time("0");
    if (!input) {
        input = "Pardubice";
    }



    fetch('http://api.openweathermap.org/data/2.5/find?q='+input+'&units=metric&APPID=0127787fdaaf2cbfe067c5e2c69e4b59')
        .then(response => {
            return response.json();
        })
        .then(data => {
            header.innerHTML = data.list[0].name + " " + Math.round(data.list[0].main.temp, 0) + "°C";

            text[0].innerHTML = data.list[0].weather[0].main;
            text[1].innerHTML = data.list[0].coord.lat + "° " + data.list[0].coord.lon + "°";
            text[2].innerHTML = "humidity: " + data.list[0].main.humidity + "% ";
            text[3].innerHTML = "current max. " + data.list[0].main.temp_max + "°C   /   min " + data.list[0].main.temp_min + "°C";

            switch((data.list[0].weather[0].main).toLowerCase()){
                case 'rain':
                img.setAttribute('src', 'assets/rain.png');
                break;

                case 'clear':
                img.setAttribute('src', 'assets/clear_sky.png');
                break;

                case 'few clouds':
                case 'scattered clouds':
                case 'broken clouds':
                img.setAttribute('src', 'assets/few_clouds.png');
                break;

                case 'clouds':
                img.setAttribute('src', 'assets/cloudy.png');
                break;

                case 'shower rain':
                img.setAttribute('src', 'assets/shower_rain.png');
                break;

                case 'rain':
                img.setAttribute('src','assets/rain.png');
                break;

                case 'thunderstorm':
                img.setAttribute('src','assets/thunderstorm.png');
                break;

                case 'snow':
                img.setAttribute('src','assets/snow.png');
                break;

                case 'mist':
                img.setAttribute('src','assets/mist.png');
                break;
            }
            

        });
    console.timeEnd("0");


    if (welcomeMsg) {
        welcomeMsg.remove();
    }
}

